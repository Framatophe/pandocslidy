---
title: "La machine à Bébert"
subtitle: "Comment inventer une machine qui ne sert à rien"
lang: fr-FR
logo: Framasoft_Logo.png
biglogo: Framasoft_Logo-500px.png
author:
- name: Bébert le mécano
  affiliation: Garage de Chimoux
date: 02 février 2023
output:
  slidy_presentation:
    css: slidy.css

...




# Youplaboum

Voici une citation :

> « La caractéristique vestimentaire du con consiste en un besoin irrésistible de s'habiller comme tout le monde. »
>
> -- <cite>Pierre Desproges</cite>


## Titre niveau 2

Monsieur Renard regardait attentivement Mimi la souris se délecter d'un bout de fromage ramené la veille.

----


# Aller à Ouagadougou planter des choux

- Un petit pois
- Deux grosses fraises

![Légende de l'image : elle peut être longue et même faire plusieurs lignes mais il vaut mieux éviter](img/FBI1967.jpg)

----

# Une petite voiture

On essaie de faire du code :

```
<div id="breakfast" class="slide section level1">
<h1>Breakfast</h1>
<p>plmop</p>

```

Voici des **éléments en gras** et en voici d'autres *en italique*.


----



# Ouvrir les tiroirs

::: {.center}

Pour centrer un paragraphe on fait comme ça

:::


- Manger du rôti de dinde selon  [cette recette](https://versnullepart.com)
- Ouvrir une porte
    - De l'intérieur vers l'extérieur
    - De l'extérieur vers l'intérieur
        - Ou bien de haut en bas
        - Ou de bas en haut

:::::::::::::: {.columns}
::: {.column width="33%"}

On peut séparer la page en trois en indiquant la largeur des colonnes.

Cela fonctionne plutôt bien

:::

::: {.column width="33%"}

Et dans cette colonne, on peut aussi marquer des trucs


:::

::: {.column width="33%"}

Et là aussi, on peut aussi marquer des trucs


:::
::::::::::::::

----

# Faire l'inventaire



- Une bielle
- un pot d'échappement

+---------------------+---------------+-----------------------+
| Objet               | Prix          | Utilisation           |
+=====================+===============+=======================+
| Bielle              | 24 €          | - Relier au piston    |
|                     |               | - Tourner en rond     |
+---------------------+---------------+-----------------------+
| Pot d'échappement   | 205 €         | - Évacuer les gazs    |
|                     |               | - Rester froid        |
+---------------------+---------------+-----------------------+

----

# Avec des colonnes

:::::::::::::: {.columns}
::: {.column width="50%"}

![Légende de l'image : elle peut être longue et même faire plusieurs lignes ce qui risque de poser problème](img/FBI1967.jpg)

:::
::: {.column width="50%"}

Commentaire sur l'image de gauche

:::
::::::::::::::




# Partie non listée dans le sommaire {.unlisted}

Mais elle est listée dans la TOC accessible en cliquant sur `index` en bas de page .

### Et ca donne quoi le titre 3 ?

Et voilà !




