# Une présentation avec Markdown + Pandoc + Slidy


Vous aurez besoin de pandoc (`sudo apt install pandoc`).

Clonez ce projet et éditez le fichier `presentation.md`, par défaut celui-ci contient des exemples de diapositives, vous pouvez vous en inspirer.

Une fois que votre fichier `presentation.md` est terminé, ouvrez un terminal dans ce dossier et utilisez pandoc comme suit.


Pour compiler avec Pandoc, la commande est la suivante :

```
pandoc -V lang=fr -t slidy --css=slidy.css --template=perso.slidy -s presentation.md -o presentation.html

```

Ou si on veut le sommaire :

```
pandoc -V lang=fr -t slidy --css=slidy.css --template=perso.slidy -s --toc --toc-depth=1 presentation.md -o presentation.html

```


- on spécifie la langue avec `-V lang=fr`
- moteur cible utilisé est slidy `-t slidy`
- on utilise une css personalisée `--css=slidy.css`
- on utilise un modèle personnel (qui intègre auteur + affiliation) `--template=perso.slidy`

# Résultat final

Ouvrez le fichier `presentation.html` avec votre navigateur.

Si vous voulez une version PDF, faites `CTRL+P` et choisissez `enregistrer au format PDF`, cochez `imprimer les arrières-plans`, puis enregistrez.

# Tripatouillage de CSS

- Le logo est défini dans le code CSS en tant que `h1::after` : il faut décommenter le bloc si on le veut.
- dans la CSS vous pouvez choisir trois styles de couleurs, il suffit de commenter / décommenter les blocs que vous voulez ou en créer d'autres en changeant les variables.
